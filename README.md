# End-to-end-examples


End-to-end-examples provides the examples that use the [DeXMS](https://gitlab.inria.fr/zefxis/DeXMS) API to generate and run a mediator in an end-to-end interaction

In order to generate a mediator, the following informations are required:
* the service's DexIDL file.
* the name of the service
* the protocol to be supported namely mqtt, coap, http, websocket
* the target service IP address and port
* the address and port to be provided by the mediator

This example contains all the above informations 

The system requirements to generate and to run the meditor:
* Ubuntu the linux based operating System 
* Java 8
* Apache Maven 3.6.0
* Git software

## Run the example mqtt-to-coap

In this example, we use a one-way end-to-end communication from a data producer using MQTT protocol to a data consumer using the CoAP protocol. Since the two end points cannot communicate directly due to the heterogeneity of the protocols, we need a mediator to bridge this protocol heterogeneity. The example contains the following java files:

* Publisher.java: the data producer that produces the random number to MQTT broker
* CoapObserver.java: the data consumer that consumes the random number
* Mediator.java uses the DeXMS api to generate the mediator. 

In the following we describe the steps to run the example. 

Install and run the MQTT broker if you do not have one. (Here we use ActiveMQ). 

* Download the ActiveMQ image: ``` docker pull rmohr/activemq```
* Start ActiveMQ: ``` docker run -p 61616:61616 -p 8161:8161 rmohr/activemq```
* By default ActiveMQ uses port number 1883 for the MQTT protocole 

Clone the End-to-end-examples repository and build the project
``` 
git clone git@gitlab.inria.fr:dexms/end-to-end-examples.git 
cd end-to-end-examples && mvn compile
```
* Open the project with an editor to edit the information before generating the mediator

Execute the following step to run  the mqtt-to-coap example:

1. To generate the mediator: 
```
cd end-to-end-examples/mqtt-to-coap
mvn exec:java -Dexec.mainClass="org.zefxis.dexms.examples.mqtt_to_coap.Mediator"
 ```
* If the maven finish succefully, the file MQTT_to_COAP.jar will be generated in the same folder (i.e. end-to-end-examples/mqtt-to-coap)

2. To execute the generated mediator:

* Make sure the MQTT broker is runing, then start the mediator:
``` 
java -jar MQTT_to_COAP.jar
```
3. To start producing data from the MQTT publisher:

```
mvn exec:java -Dexec.mainClass="org.zefxis.dexms.examples.mqtt_to_coap.Publisher"
```
4. To start consumming data on the Coap side

```
mvn exec:java -Dexec.mainClass="org.zefxis.dexms.examples.mqtt_to_coap.CoapObserver"
```

You can package the data producer and data consumer into jar files to run the example in different computing nodes. To this end make sure you set the IP addresses and the port numbers accordingly. 


# Who can I contact?

Patient NTUMBA: patient.ntumba AT inria.fr 

Georgios Bouloukakis: Georgios.Bouloukakis AT inria.fr 

Nikolaos Georgantas: nikolaos.georgantas AT inria.fr 


