package org.zefxis.dexms.examples.mqtt_to_coap;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.zefxis.dexms.gmdl.utils.enums.ProtocolType;
import org.zefxis.dexms.mediator.generator.MediatorGenerator;
import org.zefxis.dexms.mediator.manager.MediatorOutput;

public class Mediator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MediatorGenerator mediator = new MediatorGenerator();

		// Corresponds to the IP address and port number of the MQTT broker
		mediator.setServiceEndpoint("172.17.0.2", "1883");

		// Corresponds to the IP address and port number of the CoAP endpoint
		mediator.setBusEndpoint("127.0.0.1", "8893");

		String gidlFile = "src/main/resources/DeXIDL/randomValue.gidl";
		MediatorOutput output = mediator.generate(gidlFile, ProtocolType.COAP, "MQTT_to_COAP");
		try {
			FileUtils.writeByteArrayToFile(new File("MQTT_to_COAP.jar"), output.jar);
		} catch (IOException e) {e.printStackTrace();}
		
	}
}
