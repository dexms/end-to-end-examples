package org.zefxis.dexms.examples.mqtt_to_coap;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;

public class CoapObserver{

	public static void main(String args[]) {

		CoapClient client = new CoapClient("coap://127.0.0.1:8893/randomValue").useCONs();

		client = client.useExecutor();

		CoapObserveRelation relation = client.observe(

				new CoapHandler() {
					@Override
					public void onLoad(CoapResponse response) {

						String content = response.getResponseText();
						System.out.println("Coap : " + content);
					}

					@Override
					public void onError() {

						System.err.println("OBSERVING FAILED (press enter to exit)");

					}
				});

		while (!relation.isCanceled()) {

			synchronized (relation) {

				try {

					relation.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
