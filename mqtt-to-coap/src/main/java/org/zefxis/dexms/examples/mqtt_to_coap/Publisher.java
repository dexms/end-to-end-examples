package org.zefxis.dexms.examples.mqtt_to_coap;

import java.util.Random;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.simple.JSONObject;

public class Publisher {
	
	public static void main(String[] args){
		MqttClient client = null;
		Random rand = new Random();	
        	try{
    			
    			client = new MqttClient("tcp://172.17.0.2:1883", MqttClient.generateClientId());
    			client.connect();
    			while(true){
    				
    				JSONObject obj = new JSONObject();
    				obj.put("randValue", ""+rand.nextDouble());
    		        String content = obj.toJSONString();
    				MqttMessage message = new MqttMessage();
        			message.setQos(0);
        			message.setPayload(content.getBytes());
        			System.out.println("Publish : "+content);
        			client.publish("randomValue", message);
        			Thread.sleep(2000);
  	
    			}
    			
    			
    			
    		}catch (MqttException e){
    			
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
	}
}
